package demo.components.counter;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.impl.ScheduledPollConsumer;

/**
 * The Counter consumer.
 */
public class CounterConsumer extends ScheduledPollConsumer {
    private final CounterEndpoint endpoint;
    private Integer counter;

    public CounterConsumer(CounterEndpoint endpoint, Processor processor) {
        super(endpoint, processor);

        this.endpoint = endpoint;
        this.counter  = this.endpoint.getStart()

        this.setDelay(this.endpoint.getDelay())
    }

    @Override
    protected int poll() throws Exception {
        Exchange exchange = endpoint.createExchange();

        // create a message body
        exchange.getIn().setBody(counter++);

        try {
            // send message to next processor in the route
            getProcessor().process(exchange);
            return 1; // number of messages polled
        } finally {
            // log exception if an exception occurred and was not handled
            if (exchange.getException() != null) {
                getExceptionHandler().handleException("Error processing exchange", exchange, exchange.getException());
            }
        }
    }
}
