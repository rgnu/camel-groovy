package test

@GrabExclude('org.codehaus.groovy:groovy-all')
@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-groovy:2.16.3')
@Grab('org.apache.camel:camel-jsonpath:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*

def ctx = new DefaultCamelContext()
def p = ctx.createProducerTemplate()

ctx.getTypeConverterRegistry().addTypeConverters(new demo.CustomTypeConverters())

ctx.addRoutes(new RouteBuilder() {
    def void configure() {
        from('direct:input')
          .onException(Exception)
            .handled(true)
            .to('log:error?showAll=true')
          .end()
          .filter().jsonpath("\$[?(@.status == 'ok')]")
          .to('log:pass?showAll=true')
        .end()
        .routeId('jsonpath-filter')
    }
})

ctx.start();

p.sendBody('direct:input', [status: "ok", text: "example"])
p.sendBody('direct:input', '{"status": "ok", "text": "pass1"}')
p.sendBody('direct:input', '{"status": "fail", "text": "nop"}')
p.sendBody('direct:input', '{"status": "ok", "text": "pass2"}')

ctx.stop();
