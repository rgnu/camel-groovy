@Grab('io.projectreactor:reactor-stream')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import reactor.Environment;
import reactor.rx.*;


Environment.initialize();

def stream = Streams.period(1).log()

stream
.consume(
  { println it},
  { println it.message },
  { println 'Complete'}
)


System.console().readLine();