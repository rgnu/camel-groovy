@Grab('org.apache.camel:camel-core:2.17.7')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*


class Event {
  String  id=UUID.randomUUID()
  String  type='Event'
  Date    createdAt=new Date()
  String  source
  Map     payload=[:]
}


class CounterEvent extends Event {

  String type='CounterEvent'

  void setCounter(Integer value) {
    payload.counter = value
  }

  Integer getCounter() {
    payload?.counter
  }

}


// Beans
class App {

    Integer counter = 0

    Event createCounterEvent() {
      return new CounterEvent( source: 'test', counter: ++this.counter)
    }

    Boolean filterOddCounterEvent(CounterEvent event) {
      return (event.counter % 2 == 0)
    }

    String formatCounterEvent(CounterEvent event) {
      return "${event.type}@${event.source} [Counter:${event.counter}] [CreatedAt:${event.createdAt}] [Id:${event.id}]"
    }

    RouteBuilder route() {
      return new RouteBuilder() {

        void configure() {
          from('timer:clock?period=1000')
          .bean(App.class, 'createCounterEvent')
          .filter().method(App.class, 'filterOddCounterEvent')
          .bean(App.class, 'formatCounterEvent')
          .log('${body}')
        }
      }
    }
}

// Vars
def camelContext = new DefaultCamelContext()
def App          = new App()

// Routes
camelContext.addRoutes(App.route())

camelContext.start();
System.console().readLine();
camelContext.stop();
