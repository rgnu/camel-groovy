package demo.components.hello;

import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;

import org.apache.camel.impl.UriEndpointComponent;

/**
 * Represents the component that manages {@link HelloEndpoint}.
 */
public class HelloComponent extends UriEndpointComponent {

    public HelloComponent() {
        super(HelloEndpoint.class);
    }

    public HelloComponent(CamelContext context) {
        super(context, HelloEndpoint.class);
    }

    protected Endpoint createEndpoint(String uri, String remaining, Map<String, Object> parameters) throws Exception {
        Endpoint endpoint = new HelloEndpoint(uri, this);
        setProperties(endpoint, parameters);
        return endpoint;
    }
}
