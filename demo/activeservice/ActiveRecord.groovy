package demo.activeservice


class ActiveRecord {

  ActiveService service
  Boolean persisted = false

  void save() {
    this.service.save(this)
  }  

  void delete() {
    this.service.delete(this)
  }
  
  Boolean isPersisted() {
    return this.persisted    
  }

  Boolean isValid() {
    return true
  }
}

