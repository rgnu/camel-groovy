@Grab('org.apache.camel:camel-core:2.17.7')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.processor.aggregate.GroupedExchangeAggregationStrategy
import demo.Rx;

class MyRoute extends RouteBuilder {

  void configure() {

    from('direct:test')
    // aggregate all using same expression
    .aggregate(constant(true))

    // batch size 3
    .completionSize(3)

    // wait for 1 seconds to aggregate
    .completionTimeout(1000)

    .forceCompletionOnStop()

    // group the exchanges so we get one single exchange containing all the others
    .groupExchanges()
    .to('log:debug?showAll=true')
  }
}

Rx.install()

def ctx = new DefaultCamelContext()
def p   = ctx.createProducerTemplate();

ctx.addRoutes(new MyRoute())

ctx.start();

p.sendBody('direct:test', 'HI 001')
p.sendBody('direct:test', 'HI 002')
p.sendBody('direct:test', 'HI 003')
p.sendBody('direct:test', 'HI 004')
sleep(2000)

ctx.stop();
