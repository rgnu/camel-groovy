@GrabExclude('org.codehaus.groovy:groovy-all')
@Grab(group='org.codehaus.gpars:gpars:1.2.0')


import groovyx.gpars.actor.Actor
import groovyx.gpars.actor.DefaultActor
import groovyx.gpars.actor.DynamicDispatchActor

import org.codehaus.groovy.runtime.NullObject


class StreamActor extends DynamicDispatchActor {

  Actor next

  Actor pipe(Actor next) {
    this.next = next

    !isActive() && this.start()

    return next
  }

  void onMessage(NullObject message) {
    next?.send message
    terminate()
  }

}

class CounterActor extends StreamActor {

    int counter = 0

    void act() {
        loop {
            next?.send counter++
            sleep(100)
        }
    }
}


class RangeActor extends StreamActor {

    int start = 0
    int stop

    void act() {
      loop {
        def msg = start < stop ? start++ : null
        println "Sending ${msg}"
        next?.send msg
        sleep(100)
      }
    }
}


class FilterActor extends StreamActor {

  void onMessage(int message) {
    if (it % 2 == 0) next?.send it
  }
}


class LogActor extends StreamActor {
  void onMessage(int msg) {
    println "Number ${it}"
  }
}


def logger  = new LogActor().start()
def filter  = new FilterActor().start()
def counter = new RangeActor(stop: 100).start()

counter.pipe(filter).pipe(logger)


counter.join()