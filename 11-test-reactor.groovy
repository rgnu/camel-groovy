@Grab('io.projectreactor:reactor-stream')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import reactor.rx.*

def counter() {
	def count = 0;
	Streams.create { subscriber ->
		sleep(1000)
		subscriber.onNext(count++)
	}
}

def range(start, stop) {
	def count = start;
	Streams.create { subscriber ->
		if (count == stop) return subscriber.onComplete()
		subscriber.onNext(count++)
	}
}

def stream  = Streams.from([1, 2, 3, 4, 5, 6]).log()
def counter = counter().log()


stream
.filter({
  it % 2 == 0
})
.consume(
  { println it}, 
  { println it.message }, 
  { println 'Complete'}
)

counter
.consume(
  { println "Counter ${it}"}, 
  {}, 
  { println 'Complete counter'}
)

System.console().readLine()
