package demo.components.counter;

import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;

import org.apache.camel.impl.UriEndpointComponent;

/**
 * Represents the component that manages {@link CounterEndpoint}.
 */
public class CounterComponent extends UriEndpointComponent {

    public CounterComponent() {
        super(CounterEndpoint.class);
    }

    public CounterComponent(CamelContext context) {
        super(context, CounterEndpoint.class);
    }

    protected Endpoint createEndpoint(String uri, String remaining, Map<String, Object> parameters) throws Exception {
        Endpoint endpoint = new CounterEndpoint(uri, this);
        setProperties(endpoint, parameters);
        return endpoint;
    }
}
