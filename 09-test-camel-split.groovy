@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.processor.aggregate.GroupedExchangeAggregationStrategy
import demo.Rx;

class MyRoute extends RouteBuilder {

  void configure() {

    from('direct:test')
    .split().body().streaming()
    .map({ println it})
  }
}

Rx.install()

def ctx = new DefaultCamelContext()
def p   = ctx.createProducerTemplate();

ctx.addRoutes(new MyRoute())

ctx.start();

p.sendBody('direct:test', ['HI 001', 'HI 002', 'HI 003'])

ctx.stop();
