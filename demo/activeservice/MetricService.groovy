package demo.activeservice

class MetricService extends ActiveService {

  def    session
  String table = "twcs"
  
  MetricService(session) {
    this.session = session
  }
  
  ActiveRecord insert(MetricRecord record) {
    record.ts = record.ts ?: new Date().time
    
    def res = this.session.execute("INSERT INTO ${this.table} (id, ts, value) VALUES ('${record.id}', ${record.ts}, ${record.value}) USING TTL 3600;")
    
    res.wasApplied() || new RuntimeException("Error Saving ${record}")
    
    return record
  }
  
  
  ActiveRecord create(Map args) {
    ActiveRecord record = args as MetricRecord
    record.service = this
    
    return record
  }
}
