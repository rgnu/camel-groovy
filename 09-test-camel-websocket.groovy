@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-websocket:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.builder.RouteBuilder
import static demo.Rx.map;
import static demo.Rx.filter;

class MyRoute extends RouteBuilder {

  def fromJson() {
    def slurper = new groovy.json.JsonSlurper();
    return {
      it.in.body = slurper.parseText(it.in.body);
    }
  }

  def toJson() {
    return {
      it.in.body = groovy.json.JsonOutput.toJson(it.in.body);
    }
  }

  void configure() {
    // expose a echo websocket client, that receive events
    from('websocket://events')
      .process(map(fromJson()))
      .filter(filter({ it.in.body.type == 'Command' && it.in.body.cmd }))
      .process(map({ it.in.headers.Command = it.in.body.cmd}))
      .log('>>> Message received from WebSocket Client : ${headers} ${body}')
      .recipientList(simple('direct:${header.Command}'));

    from('direct:getDate')
    .process(map({ it.in.body = new Date()}))
    .log('>>> getDate ${body} ${headers}')
    .to('websocket://events')
  }
}

def ctx = new DefaultCamelContext()
def p   = ctx.createProducerTemplate();

ctx.addRoutes(new MyRoute())

ctx.start();
System.console().readLine();
ctx.stop();
