@Grab('org.slf4j:slf4j-log4j12:1.7.12')
@Grab('com.hazelcast:hazelcast:3.8.4')

import com.hazelcast.config.*
import com.hazelcast.core.Hazelcast
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.core.IQueue

def seed  = 'localhost'
def conf  = new Config()
def joins = conf.getNetworkConfig().getJoin()

joins.getMulticastConfig().setEnabled(false);
joins.getTcpIpConfig().setEnabled(true).addMember(seed);

HazelcastInstance instance = Hazelcast.newHazelcastInstance(conf);

IQueue<Integer> queue = instance.getQueue( "queue" );

for ( int k = 1; k < 100; k++ ) {
  queue.put( k );
  System.out.println( "Producing: " + k );
  Thread.sleep(100);
}
queue.put( -1 );
System.out.println( "Producer Finished!" );

instance.shutdown()
