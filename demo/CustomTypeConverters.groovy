package demo

import org.apache.camel.*


class CustomTypeConverters implements  TypeConverters {

    @Converter
    public String mapToString(java.util.LinkedHashMap body) {
      return groovy.json.JsonOutput.toJson(body);
    }

    @Converter
    public java.io.InputStream mapToInputStream(java.util.LinkedHashMap body) {
      return new java.io.StringBufferInputStream(mapToString(body));
    }

    @Converter
    public java.io.InputStream stringToInputStream(String body) {
      return new java.io.StringBufferInputStream(body);
    }

}
