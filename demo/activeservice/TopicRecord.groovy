package demo.activeservice

class TopicRecord extends ActiveRecord {

  String topic
  UUID   offset
  String data
  
  @Override
  String toString() {
    "TopicRecord [" +
    "topic: ${topic}, " +
    "offset: ${offset}, " + 
    "data: ${data}" +
    "]"
  }
}

