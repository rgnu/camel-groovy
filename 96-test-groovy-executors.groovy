package test

import java.util.concurrent.*;


class Counter {
  def start = 0
  def stop  = 10

  Iterator iterator() {

    def counter = start

    return [
      hasNext: { counter < stop },
      next: { counter += 1 }
    ] as Iterator
  }
}


class Flow {

  static def parallel(num) {
    def exec = Executors.newFixedThreadPool(num);

    return { Callable task ->
      return exec.submit((Callable) task)
    }
  }


  static def flatMap() {
    return { Future it ->
      it.get()
    }
  }

  static def slowTask(time) {
    return { Integer num ->
      return {
        def pid = Thread.currentThread().id
        println "Running $num into thread $pid take $time"

        sleep(time)

        return num * num
      } as Callable<Integer>
    }
  }

  static def log() {
    return {
      println "Log: $it"
    }
  }
}


new Counter(stop: 20)
  .collect(Flow.slowTask(1000))
  .collect(Flow.parallel(5))
  .collect(Flow.flatMap())
  .each(Flow.log())

//exec.shutdown()
