@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.Exchange
import org.apache.camel.Processor

import demo.Rx;

class MyRoute extends RouteBuilder {

  void configure() {
    onException(Exception.class)
      .onRedelivery(Rx.map({ println "Redelivery ${it} ${it.in.headers}"}))
      .process(Rx.map({ println "ERROR ${it} ${it.in.headers} ${it.out.headers}"}))
      .maximumRedeliveries(3)
      .redeliveryDelay(100)
      .handled(true)
    .end();

    from('direct:test')
    .map({ it.in.headers.breadcrumbId = new UUID(new Date().time << 16 + 0x0100, 0)})
    .map({ println it })
    .throwException(new Exception("BOOM"))
    .to('log:output')
  }
}

Rx.install()

def ctx = new DefaultCamelContext()
def p   = ctx.createProducerTemplate();

ctx.addRoutes(new MyRoute())

ctx.start();

p.sendBody('direct:test', ['HI 001', 'HI 002', 'HI 003'])

ctx.stop();
