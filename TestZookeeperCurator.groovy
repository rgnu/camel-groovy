@Grab('org.apache.curator:curator-framework:[2.7.1,2.8)')
@Grab('org.slf4j:slf4j-simple:1.7.12')


import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.retry.ExponentialBackoffRetry;
import groovy.json.JsonSlurper

def zkUri    = new URI(System.properties.get('zk.uri', 'zk://localhost:2181/'))
def zkClient = CuratorFrameworkFactory.newClient(zkUri.authority, new ExponentialBackoffRetry(1000, 3))

def toJson() {
  def parser = new JsonSlurper()

  return {
    parser.parse(it) as Map
  }
}

def findAll(client) {
  return { path ->
    assert path instanceof String

    client.children.forPath(path).collect { "${path}/${it}" as String }
  }
}

def findById(client) {
  return {
    assert it instanceof String

    client.data.forPath(it)
  }

}

zkClient.start()

[zkUri.path, '/glue']
.iterator()
.collect(findAll(zkClient))
.flatten()
.collect(findById(zkClient))
.collect(toJson())
.grep { it instanceof Map }
.each { it ->
  println it
}
