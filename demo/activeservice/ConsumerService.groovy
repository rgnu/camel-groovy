package demo.activeservice

class ConsumerService extends ActiveService {

  def    session
  String table = "consumer"
  
  
  ConsumerService(session) {
    this.session = session
  }
  
  ActiveRecord insert(ConsumerRecord record) {
    record.offset = record.offset ?: UUIDs.timeBased()
    
    def res = this.session.execute("INSERT INTO ${this.table} (id, topic, offset) VALUES ('${record.id}', '${record.topic}', ${record.offset}, '${record.data}') USING TTL 3600;")
    
    res.wasApplied() || new RuntimeException("Error Saving ${record}")

    return record
  }

  ActiveRecord create(Map args) {
    ActiveRecord record = args as ConsumerRecord
    record.service = this
    
    return record
  }
}

