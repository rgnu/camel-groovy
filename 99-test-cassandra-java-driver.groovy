@Grab('com.datastax.cassandra:cassandra-driver-core:3.0.7')
//@Grab('org.apache.cassandra:cassandra-all:1.2.5')

import com.datastax.driver.core.Cluster
import com.datastax.driver.core.Host
import com.datastax.driver.core.Metadata

import com.datastax.driver.core.BoundStatement
import com.datastax.driver.core.PreparedStatement

def cluster = Cluster.builder().addContactPoint('cassandra.cassandra.docker').withPort(9042).build()

class User {
  UUID id
  String name
  Integer age
  String city

  static User fromRow(row) {
    [
      id: row.getUUID('id'),
      name: row.getString('name'),
      age:  row.getInt('age'),
      city: row.getString('city')
    ] as User
  }

  @Override
  String toString() {
    "User [" +
    "id: ${id}, " +
    "name: ${name}, " + 
    "age: ${age}, " +
    "city: ${city}" +
    "]"
  }
}

try {
    def metadata = cluster.getMetadata()
    println("Connected to cluster: ${metadata.clusterName}")

    for (host in metadata.allHosts) {
        println("Datacenter: $host.datacenter, Host: $host.address, Rack: $host.rack")
    }

    def session = cluster.connect('movielens')

    println('----- Execute Statement -----')
    for (row in session.execute('SELECT * FROM users LIMIT 10')) {
      println(User.fromRow(row))
    }

    def preparedStatement = session.prepare('SELECT * FROM users WHERE id = ?')
    def boundStatement = new BoundStatement(preparedStatement)

    println('----- Execute PreparedStatement -----')
    for (row in session.execute(boundStatement.bind(UUID.fromString('b52fcdfc-0eaf-4432-9896-aa22db56edb2')))) {
      println(User.fromRow(row))
    }

} finally {
    cluster.close()
}
