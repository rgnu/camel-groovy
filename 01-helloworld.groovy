@Grab('org.apache.camel:camel-core:2.13.1')
@Grab('org.apache.camel:camel-netty:2.13.1')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*

def camelContext = new DefaultCamelContext()
camelContext.addRoutes(new RouteBuilder() {
    def void configure() {
        from("timer://jdkTimer?period=3000")
            .to("log://camelLogger?level=INFO")
            .process(new Processor() {
                def void process(Exchange exchange) {
                    println("Hello World!")
                    exchange.in.body = 'test'
                }
            })
            .to('netty:tcp://localhost:8125?textline=true')
    }
})

camelContext.start();
System.console().readLine();
camelContext.stop();
