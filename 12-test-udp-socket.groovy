addr = InetAddress.getByName("localhost")
port = 8125

socket = new DatagramSocket()

while (true){
    random    = new Random()
    randomInt = random.nextInt(10)

    data = ("test-groovy:"+randomInt+"|ms").getBytes("ASCII")

    packet = new DatagramPacket(data, data.length, addr, port)
    socket.send(packet)
    sleep(250)
}
