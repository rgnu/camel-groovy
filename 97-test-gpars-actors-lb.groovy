@GrabExclude('org.codehaus.groovy:groovy-all')
@Grab('org.codehaus.gpars:gpars:1.2.0')

import groovyx.gpars.actor.*
import groovyx.gpars.group.DefaultPGroup
import groovyx.gpars.group.NonDaemonPGroup

def poolSize = 20
def pool     = new DefaultPGroup(poolSize)

def actors = (1..poolSize).collect {pool.messageHandler {
    when { Integer msg ->
        println("I'm number ${msg} on thread ${Thread.currentThread().name}")
        Thread.sleep(1000)
    }
}}


def integers = (1..poolSize*4)

integers.each {
  actors[it % poolSize] << it
  println "Sending $it"
}

pool.shutdown()