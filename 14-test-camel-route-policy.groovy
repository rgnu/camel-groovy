@Grab('org.scala-lang:scala-library:2.11.8')

@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-zookeeper:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')


import org.apache.camel.*
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.zookeeper.policy.ZooKeeperRoutePolicy;

class TestRoutePolicyRoute extends RouteBuilder {

  @Override
  void configure() {
    def zk          = new URI(System.properties.get('zookeeper.uri', 'zookeeper://localhost:2181'))

    from('timer:clock')
    .routePolicy(new ZooKeeperRoutePolicy("zookeeper://${zk.host}:${zk.port}/test/lock", 1))
    .to('log:output?showHeaders=true');

  }
}


def ctx = new DefaultCamelContext()

// Event Posting Rest Service
ctx.addRoutes(new TestRoutePolicyRoute())

ctx.start();
System.console().readLine();
ctx.stop();
