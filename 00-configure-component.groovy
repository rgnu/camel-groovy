@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-zookeeper:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import org.apache.camel.component.zookeeper.ZooKeeperComponent


class TestRouter extends RouteBuilder {
  @Override
  void configure() {

    from('zk:%20/test/somenode?repeat=true&listChildren=true')
    .to('log:input?showAll=true')

  }
}


def camelContext = new DefaultCamelContext()
def zkComponent  = new ZooKeeperComponent()

zkComponent
.getConfiguration()
.addZookeeperServer('localhost:2181')

camelContext.addComponent('zk', zkComponent)
camelContext.addRoutes(new TestRouter())

camelContext.start();
System.console().readLine();
camelContext.stop();
