@Grab('org.apache.camel:camel-core:2.13.1')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import org.apache.camel.util.jndi.*

def jndiContext  = new JndiContext();
def camelContext = new DefaultCamelContext(jndiContext)

// Beans
class SystemInfoService {
    def void run() {
        println("Hello World!")
    }
}

jndiContext.bind("systemInfoPoller", new SystemInfoService())

// Routes
camelContext.addRoutes(new RouteBuilder() {
    def void configure() {
        from("timer://jdkTimer?period=3000")
            .to("log://camelLogger?level=INFO")
            .to("bean://systemInfoPoller?method=run")
    }
})

camelContext.start();
System.console().readLine();
camelContext.stop();
