package demo.components.counter;

import org.apache.camel.Consumer;
import org.apache.camel.Processor;
import org.apache.camel.Producer;
import org.apache.camel.impl.DefaultEndpoint;
import org.apache.camel.spi.Metadata;
import org.apache.camel.spi.UriEndpoint;
import org.apache.camel.spi.UriParam;
import org.apache.camel.spi.UriPath;

/**
 * Represents a Counter endpoint.
 */
@UriEndpoint(scheme = "counter", title = "Counter", syntax="counter:name", consumerClass = CounterConsumer.class, label = "Counter")
public class CounterEndpoint extends DefaultEndpoint {

    @UriPath @Metadata(required = "true")
    private String name;

    @UriParam(defaultValue = "1000")
    private int delay = 1000;

    @UriParam(defaultValue = "0")
    private int start = 0;

    public CounterEndpoint() {
    }

    public CounterEndpoint(String uri, CounterComponent component) {
        super(uri, component);
    }

    public CounterEndpoint(String endpointUri) {
        super(endpointUri);
    }

    public Producer createProducer() throws Exception {
        return new CounterProducer(this);
    }

    public Consumer createConsumer(Processor processor) throws Exception {
        return new CounterConsumer(this, processor);
    }

    public boolean isSingleton() {
        return true;
    }

    /**
     * Some description of this option, and what it does
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * Some description of this option, and what it does
     */
    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getDelay() {
        return delay;
    }

    /**
     * Some description of this option, and what it does
     */
    public void setStart(int start) {
        this.start = start;
    }

    public int getStart() {
        return start;
    }

}
