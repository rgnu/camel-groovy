class Counter {
   def start = 0
   def stop  = 10

   Iterator iterator() {

     def counter = start

     return [
       hasNext: { counter < stop },
       next: { counter += 1 }
     ] as Iterator
   }
}


class Fib {
   def stop  = 10

   Iterator iterator() {

     def prev = 1
     def acc  = 1

     return [
       hasNext: { acc < stop },
       next: { def aux = prev; prev = acc; acc += aux; aux }
     ] as Iterator
   }
}

def counter = new Fib(stop: 50)

counter.each { println "GET ${it}"}
