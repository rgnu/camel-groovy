@Grab('org.apache.camel:camel-core:2.13.1')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*

// Vars
def camelContext = new DefaultCamelContext()

// Beans
class SystemInfoService {
    @Handler
    def void run() {
        println("Hello World!")
    }
}

// Routes
camelContext.addRoutes(new RouteBuilder() {
    def void configure() {
        from("timer://jdkTimer?period=3000")
            .to("log://camelLogger?level=INFO")
            .bean(SystemInfoService.class)
    }
})

camelContext.start();
System.console().readLine();
camelContext.stop();
