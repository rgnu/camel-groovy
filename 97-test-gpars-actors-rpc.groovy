@GrabExclude('org.codehaus.groovy:groovy-all')
@Grab('org.codehaus.gpars:gpars:1.2.0')

import groovyx.gpars.actor.*

class TooLarge {}
class TooSmall {}
class YouWin {}
class RangeNumberRequest {}
class RangeNumberReply { int min, max}


class GameMaster extends DefaultActor {

    int min, max, secretNum

    void afterStart() {
        min       = 0
        max       = 100000
        secretNum = new Random().nextInt(max)
    }


    void act() {
        loop { react { handleMessage(it) } }
    }


    void handleMessage(RangeNumberRequest msg) {
        reply new RangeNumberReply(min: min, max: max)
    }


    void handleMessage(int num) {

        if (num > secretNum)
            reply new TooLarge()
        else if (num < secretNum)
            reply new TooSmall()
        else {
            reply new YouWin()
            terminate()
        }
    }
}


class Player extends DefaultActor {

    String name
    Actor server

    int myNum
    int counter = 0
    int PID, start, stop

    void afterStart() {
      def range = server.sendAndWait(new RangeNumberRequest())
      
      start = range.min
      stop  = range.max
      PID   = Thread.currentThread().id

      println "[$PID] $name: Start [$start, $stop]"; 

    }

    void act() {
      loop {
        myNum = new Random().nextInt(stop - start) + start
        counter++

        server << this.myNum

        react { handleMessage(it) }
      }
    }

    void handleMessage(YouWin msg) {
      println "[$PID] $name: I won $myNum before $counter iter"; 
      terminate();
    }

    void handleMessage(TooLarge msg) {
      stop = myNum
      println "[$PID] $name: $myNum was too large";
    }

    void handleMessage(TooSmall msg) {
      start = myNum
      println "[$PID] $name: $myNum was too small";
    }

    void handleMessage(Object msg) {
      throw new Exception("Unhandled message type $msg")
    }

}

def master  = new GameMaster().start()
def players = []

players << new Player(name: 'Player1', server: master).start()
players << new Player(name: 'Player2', server: master).start()

//this forces main thread to live until both actors stop
master.join()

