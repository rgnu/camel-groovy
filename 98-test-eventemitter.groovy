import demo.events.*

class LoggerListener implements IListener {

    private String id;

    LoggerListener() {
      this(UUID.randomUUID().toString());
    }

    LoggerListener(String id) {
      this.id = id
    }

    String getId() {
      return id;
    }

    void handle(Object event) {
      println "${id} receive Object ${event}";
    }

    void handle(String event) {
      println "${id} receive String ${event}";
    }
}

IEventEmitter eventEmitter = new EventEmitterImpl();
IListener l1 = new LoggerListener();
IListener l2 = new LoggerListener('l2');

eventEmitter.addListener('test', l1);
eventEmitter.addListener('test', l2);

eventEmitter.emit('test', [foo: 'bar']);
eventEmitter.emit('test', 'foo=bar');
