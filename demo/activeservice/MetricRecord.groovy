package demo.activeservice

class MetricRecord extends ActiveRecord {

  String id
  BigInteger ts
  BigInteger value

  @Override
  String toString() {
    "Metric [" +
    "id: ${id}, " +
    "ts: ${ts}, " + 
    "value: ${value}" +
    "]"
  }

}

