@Grab('org.scala-lang:scala-library:2.11.8')

@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')


import org.apache.camel.*
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.builder.RouteBuilder;

import static demo.Rx.map;
import static demo.Rx.filter;


class TestCronRoute extends RouteBuilder {

  def timerFilter(String test) {
    filter({
      it.in.headers.firedTime.format('MMddHHmmssu').matches(test)
    })
  }

  @Override
  void configure() {

    from('timer:clock').to('seda:clock?multipleConsumers=true');

    from('seda:clock?multipleConsumers=true')
      .filter(timerFilter('.........0.'))
      .to('log:output?showHeaders=true')
    .end()
    .routeId('every00s');

    from('seda:clock?multipleConsumers=true')
      .filter(timerFilter('.........5.'))
      .to('log:output?showHeaders=true')
    .end()
    .routeId('every05s');


  }
}






def ctx = new DefaultCamelContext()

// Event Posting Rest Service
ctx.addRoutes(new TestCronRoute())

ctx.start();
System.console().readLine();
ctx.stop();
