@Grab('org.apache.camel:camel-core:2.16.5')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import demo.Rx

Rx.install()

def ctx = new DefaultCamelContext()
ctx.setMessageHistory(false)
ctx.setAllowUseOriginalMessage(false)
ctx.disableJMX()

ctx.addRoutes(new RouteBuilder() {
    def void configure() {
        from('timer:clock?period=1000')
        .map(Rx.counter(0))
        .map({ it.properties.firedTime = it.in.headers.firedTime })
        .filter({ return it.in.body % 2 == 0})
        .map(Rx.log())
        .to('log:out?showAll=true')
    }
})

ctx.start();
System.console().readLine();
ctx.stop();
