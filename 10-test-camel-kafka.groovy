@Grab('org.scala-lang:scala-library:2.11.8')

@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-jetty:2.16.3')
@Grab('org.apache.camel:camel-kafka:2.16.3')
@Grab('org.apache.camel:camel-jackson:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')


import org.apache.camel.*
import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.kafka.KafkaConstants
import org.apache.camel.model.dataformat.JsonLibrary


class KafkaRoute extends RouteBuilder {

  @Override
  void configure() {
    def kafka = new URI(System.properties.get('kafka.uri', 'kafka://localhost:9092'))
    def zk    = new URI(System.properties.get('zookeeper.uri', 'zookeeper://localhost:2181'))

    from('timer:clock?period=2000')
    .bean(this, 'newMessage')
    .marshal().json(JsonLibrary.Jackson).convertBodyTo(String.class)
    .setHeader(KafkaConstants.PARTITION_KEY, constant('0'))
    .to("kafka:${kafka.host}:${kafka.port}?topic=clock&zookeeperConnect=${zk.host}:${zk.port}&serializerClass=kafka.serializer.StringEncoder")
    .routeId('events.clock.tx')

    from("kafka:?topic=clock&zookeeperConnect=${zk.host}:${zk.port}&groupId=test")
    .unmarshal().json(JsonLibrary.Jackson, Map.class)
    .bean(this, 'timeDiff')
    .to('log:input')
    .routeId('events.clock.rx')

  }

  Map timeDiff(@Body Map message) {
    message.diff = new Date().getTime() - message.timestamp
    return message
  }

  Map newMessage(@Header("breadcrumbId") String id, Exchange e) {
    return [ kind: 'Clock', id: id, timestamp: new Date().getTime() ]
  }
}


def ctx = new DefaultCamelContext()

// Event Posting Rest Service
ctx.addRoutes(new KafkaRoute())

ctx.start();
System.console().readLine();
ctx.stop();
