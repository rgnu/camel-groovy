@Grab('org.apache.camel:camel-core:2.13.1')
@Grab('org.apache.camel:camel-groovy:2.13.1')
@Grab('org.apache.camel:camel-jetty:2.13.1')
@Grab('org.apache.camel:camel-jackson:2.13.1')
@Grab('org.slf4j:slf4j-jdk14:1.7.5')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import org.apache.camel.model.dataformat.JsonLibrary


class Event {
	String id
	String kind
	Map metadata
	Map payload

	String toString() {
		return "[Kind:${kind}] [Payload:${payload}]"
	}
}

// Vars
def camelContext = new DefaultCamelContext();

// Routes
camelContext.addRoutes(new RouteBuilder() {
	void configure() {
		from("jetty:http://0.0.0.0:8090/events")
			.unmarshal().json(JsonLibrary.Jackson, Event.class)
			.filter().simple('${body.kind} == "test"')
			.marshal().json(JsonLibrary.Jackson)
			.to("log://input?showAll=true")
	}
});

camelContext.start();
System.console().readLine();
camelContext.stop();
