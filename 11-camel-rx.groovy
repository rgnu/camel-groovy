@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-jackson:2.16.3')
@Grab('org.apache.camel:camel-rx:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import org.apache.camel.rx.*
import org.apache.camel.rx.support.EndpointObservable
import rx.functions.*
import rx.Observable
import rx.Observer


class Op {

	static counter(start) {
		def count = start ?: 0
		return {
			count++
		}
	}

	static getTime() {
		return {
			it.headers.firedTime
		}
	}

	static odd() {
		return {
			(it % 2 == 0)
		}
	}

	static log(msg) {
		return {
			println "$msg: $it"
		}
	}
}

// Vars
CamelContext camelContext = new DefaultCamelContext()
ReactiveCamel rx = new ReactiveCamel(camelContext)

camelContext.start();


def timer   = rx.toObservable('timer:test?period=1000')
def counter = timer.map(Op.counter()).publish()
def odd     = counter.filter(Op.odd()).publish()
def log     = rx.subjects.ReplaySubject.create()

log.subscribe(Op.log('Logger'))

counter.subscribe(Op.log('Counter'))
counter.connect()

odd.subscribe(Op.log('Odd    '))
odd.connect()

System.console().readLine();
camelContext.stop();
