@GrabExclude('org.codehaus.groovy:groovy-all')
@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-groovy:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.builder.RouteBuilder

class MyRoute extends RouteBuilder {

  void configure() {

    from('direct:test')
    .process({ println it})

  }
}

def ctx = new DefaultCamelContext()
def p   = ctx.createProducerTemplate();

ctx.addRoutes(new MyRoute())

ctx.start();

p.sendBody('direct:test', 'Hi 001')
p.sendBody('direct:test', 'Hi 002')
p.sendBody('direct:test', 'Hi 003')

ctx.stop();
