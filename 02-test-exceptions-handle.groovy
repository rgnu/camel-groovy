package example

@Grab('org.apache.camel:camel-core:2.16.5')
@Grab('org.apache.camel:camel-jetty:2.16.5')
@Grab('org.apache.camel:camel-jackson:2.16.5')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*

import org.apache.camel.model.rest.RestBindingMode

import groovy.util.logging.Slf4j
import groovy.transform.InheritConstructors


// Exceptions
@InheritConstructors
class NotFoundException extends Exception {}

@InheritConstructors
class ConnectionException extends Exception {}


// Beans
@Slf4j
class BaseHandler implements Processor {

    @Override
    void process(Exchange ex) {

      Exception exception = ex.getProperty(Exchange.EXCEPTION_CAUGHT);

      try {

        log.debug("Process [Handler:${this}] [Body:${ex.in.body}] [Exception:${exception}]")

        if (!exception)
          this.onSuccess(ex)
        else
          this.onError(exception, ex)

        exception = null
      } catch(Exception e) {
        exception = e
      }

      ex.setProperty(Exchange.EXCEPTION_CAUGHT, exception);
    }

    void onSuccess(Exchange ex) {}

    void onError(Exception exception, Exchange ex) {
      throw exception
    }
}


@Slf4j
class CounterService extends BaseHandler {

    def start = 0
    def step  = 1

    void onSuccess(Exchange ex) {
      start += step
      ex.out.body = start
    }
}

@Slf4j
class SleeperService extends BaseHandler {
    def time

    void onSuccess(Exchange ex) {
        log.info("Sleeping ${time}ms ...")
        sleep(this.time);
    }
}


@Slf4j
class TimerStartService extends BaseHandler {

    def name

    void onSuccess(Exchange ex) {
        ex.properties[name] = new Date().time
    }
}


@Slf4j
class TimerStopService extends BaseHandler {

    def name

    void onSuccess(Exchange ex) {
        def time = new Date().time - ex.properties[name]
        log.info("Timer:${name} Lag:${time}...")
    }
}


@Slf4j
class ThrowException extends BaseHandler {

    def exception

    void onSuccess(Exchange ex) {
      log.info("[Throwing:${exception}]...")
      throw exception
    }
}


@Slf4j
class Logger extends BaseHandler {

  void onSuccess(Exchange ex) {
    log.info("[Message:${ex}]...")
  }
}


@Slf4j
class RandomError extends BaseHandler {

    Exception  exception
    BigDecimal limit

    RandomError() {
      this(0.5, new Exception('Unknow error'))
    }

    RandomError(BigDecimal limit) {
      this(limit, new Exception('Unknow error'))
    }

    RandomError(BigDecimal limit, Exception exception) {
      this.limit     = limit
      this.exception = exception
    }

    void onSuccess(Exchange ex) {
      if (Math.random() < this.limit) throw exception
    }

    String toString() {
      return "${this.class.name}[limit:${this.limit} exception:${this.exception}]"
    }
}


@Slf4j
class Response extends BaseHandler {

  void onSuccess(Exchange ex) {
    log.info("Handle OK ...")

    ex.out.setHeader(Exchange.CONTENT_TYPE, 'application/vnd.counter+json');
    ex.out.body = [ status: 'ok', counter: ex.in.body, deliveryTime:  new Date().time - ex.properties['timerTest'] ];
  }

  void onError(Exception exception, Exchange ex) {
    log.error("[Throwed Exception:${exception}]...")

    ex.out.setHeader(Exchange.HTTP_RESPONSE_CODE, 500);
    ex.out.setHeader(Exchange.CONTENT_TYPE, 'application/vnd.error+json');
    ex.out.body = [ status: 'error', message: exception.message ];
  }

  void onError(NotFoundException exception, Exchange ex) {
    log.error("Handle NotFound [Throwed Exception:${exception}] ...")
    ex.out.setHeader(Exchange.HTTP_RESPONSE_CODE, 404);
    ex.out.setHeader(Exchange.CONTENT_TYPE, 'application/vnd.error+json');
    ex.out.body = null;
  }

}


// Vars
def camelContext = new DefaultCamelContext()
camelContext.setMessageHistory(false)
camelContext.setAllowUseOriginalMessage(false)
camelContext.disableJMX()


// Routes
camelContext.addRoutes(new RouteBuilder() {

  void configure() {

    restConfiguration()
      .component('jetty')
      .host('0.0.0.0')
      .port(8081)
      .bindingMode(RestBindingMode.auto)
      .skipBindingOnErrorCode(false)

    onException(Exception)
      .continued(true)

    rest('/')
      .get()
      .route()
        .bean(new TimerStartService(name: 'timerTest'))
        .bean(new RandomError(0.25, new Exception('Fatal Error')))
        .bean(new RandomError(0.25, new NotFoundException('Not Found')))
        .bean(new RandomError(0.10, new ConnectionException('Connection error')))
        .bean(new CounterService())
        .bean(new SleeperService(time: 100))
        .bean(new TimerStopService(name: 'timerTest'))
        .bean(new Response())
      .end()
  }
})

camelContext.start();
System.console().readLine();
camelContext.stop();
