package demo.activeservice

class ConsumerRecord extends ActiveRecord {

  String id
  String topic
  UUID   offset
  
  @Override
  String toString() {
    "ConsumerRecord [" +
    "id: ${id}, " +
    "topic: ${topic}, " +
    "offset: ${offset}, " + 
    "]"
  }
}
