@Grab('org.apache.camel:camel-core:2.13.1')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import org.apache.camel.util.jndi.*

// Vars
def jndiContext  = new JndiContext();
def camelContext = new DefaultCamelContext(jndiContext)
def dataDir      = "/${System.properties['user.dir']}/test/file-poller-demo"

// Beans
class UpperCaseTextService {
    def String transform(String text) {
        return text.toUpperCase()
    }
}

jndiContext.bind("upperCaseTextService", new UpperCaseTextService())

// Routes
camelContext.addRoutes(new RouteBuilder() {
    def void configure() {
        from("file://${dataDir}/in")
            .to("log://camelLogger")
            .to("bean://upperCaseTextService?method=transform")
            .to("file://${dataDir}/out")
    }
})

camelContext.start();
System.console().readLine();
camelContext.stop();
