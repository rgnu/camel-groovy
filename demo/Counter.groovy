package demo

import org.apache.camel.*

class Counter {
  Integer counter = 0

  @Handler
  String run() {
    return "${++counter}"
  }
}
