/*
    CREATE KEYSPACE IF NOT EXISTS twcs WITH replication = {
        'class': 'SimpleStrategy',
        'replication_factor': '1'
    }  AND durable_writes = true;

    CREATE TABLE IF NOT EXISTS twcs.twcs (
        id text,
        ts bigint,
        value bigint,
        PRIMARY KEY (id, ts)
    ) WITH CLUSTERING ORDER BY (ts ASC)
        AND compaction = {
            'class': 'org.apache.cassandra.db.compaction.TimeWindowCompactionStrategy',
            'compaction_window_size': '10',
            'compaction_window_unit': 'MINUTES',
            'max_threshold': '32',
            'min_threshold': '4'
        }
        AND default_time_to_live = 3600
        AND gc_grace_seconds = 60;

*/

@Grab('com.datastax.cassandra:cassandra-driver-core:3.0.7')

import com.datastax.driver.core.Cluster
import com.datastax.driver.core.Host
import com.datastax.driver.core.Metadata

import com.datastax.driver.core.BoundStatement
import com.datastax.driver.core.PreparedStatement

import demo.activeservice.*


def cluster = Cluster.builder().addContactPoint("cassandra.cassandra.docker").withPort(9042).build()

try {

    def session       = cluster.connect("twcs")
    def metricService = new MetricService(session)
    def random        = new Random()
    def now, timeDiff;
    
    while (true) {
      now = new Date().time
      
      metricService.create([id: "runtime.freeMemory", ts: now, value: Runtime.runtime.freeMemory()]).save()
      metricService.create([id: "runtime.totalMemory", ts: now, value: Runtime.runtime.totalMemory()]).save()
      metricService.create([id: "runtime.maxMemory", ts: now, value: Runtime.runtime.maxMemory()]).save()     

      (1..1000).each { 
        metricService.create([id: "random.${it}", ts: now, value: random.nextInt(10000)]).save()      
      }
      
      timeDiff = new Date().time - now

      metricService.create([id: "metric.time", ts: now, value: timeDiff]).save()   

      println("Sleeping 1000ms TimeDiff:${timeDiff}")
      sleep(1000)
    }

} finally {
    cluster?.close()
}
