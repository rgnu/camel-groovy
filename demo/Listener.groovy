package demo

@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.codehaus.groovy.modules.http-builder:http-builder:0.7')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.Exchange

import groovyx.net.http.HTTPBuilder
import static groovyx.net.http.ContentType.JSON

import groovy.util.logging.Slf4j


@Slf4j
class Listener {

  static notify(callback) {
    def http = new HTTPBuilder(callback)

    return Rx.ClosureProcessor({
      try {
        http.post(body: it.in.body, requestContentType: JSON) { resp, body -> }
      } catch (Exception e) {
        it.exception = e
      }
    })
  }

  static onSuccess() {

    return Rx.ClosureProcessor({
      log.info "Id:${it.exchangeId} Body:${it.in.body} Status:Ok"
    })
  }

  static onError() {
    return Rx.ClosureProcessor({
      Exception cause = it.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
      log.error "Id:${it.exchangeId} Body:${it.in.body} Status:Error (${cause.message})"
    })
  }
}
