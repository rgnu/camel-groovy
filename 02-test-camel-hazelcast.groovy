@Grab('org.apache.camel:camel-core:2.16.5')
@Grab('org.apache.camel:camel-jetty:2.16.5')
@Grab('org.apache.camel:camel-jackson:2.16.5')
@Grab('org.apache.camel:camel-hazelcast:2.16.5')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import org.apache.camel.model.dataformat.JsonLibrary

Processor counter() {
  Integer counter = 0

  new Processor() { void process(Exchange ex) { ex.out.body = counter++ }}
}



def ctx         = new DefaultCamelContext()
def queueEvents = ctx.getEndpoint('hazelcast:seda:events?concurrentConsumers=10')
def restEvents  = ctx.getEndpoint('jetty:http://0.0.0.0:8080/events')
def restCounter = ctx.getEndpoint('jetty:http://0.0.0.0:8080/counter')
def logger      = ctx.getEndpoint('log:logger?showAll=true')

/*
ctx.addRoutes(new RouteBuilder() {
	void configure() {
		from(restEvents)
		    .unmarshal().json(JsonLibrary.Jackson, Map)
			.to(queueEvents)
	}
});
*/

ctx.addRoutes(new RouteBuilder() {
    void configure() {
        from(queueEvents)
        .to(logger)
    }
})

ctx.start()
addShutdownHook { ctx.stop() }
Thread.currentThread().join()
