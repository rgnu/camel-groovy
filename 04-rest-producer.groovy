@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.codehaus.groovy.modules.http-builder:http-builder:0.7')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*

import demo.Listener


// Vars
def ctx = new DefaultCamelContext()
def p   = ctx.createProducerTemplate()

// Routes
ctx.addRoutes(new RouteBuilder() {

  void configure() {

    from('direct:test')
    .onException(Exception.class).handled(true)
      .process(Listener.onError())
    .end()
    .process(Listener.notify('http://localhost:3000/event/test'))
    .process(Listener.onSuccess())
  }

});

ctx.start();

p.sendBody('direct:test', [ test: 'Hi 001' ] as Map)
p.sendBody('direct:test', [ test: 'Hi 002' ] as Map)
p.sendBody('direct:test', [ test: 'Hi 003' ] as Map)

ctx.stop();
