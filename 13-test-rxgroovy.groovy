@GrabExclude('org.codehaus.groovy:groovy-all')
@Grab('io.reactivex:rxgroovy')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.Subscriptions;
import rx.functions.Action0
import rx.functions.Func1;

/**
 * Fetch a list of Wikipedia articles asynchronously with error handling.
 *
 * @param wikipediaArticleName
 * @return Observable<String> of HTML
 */
def fetchWikipediaArticleAsynchronouslyWithErrorHandling(String... wikipediaArticleNames) {
    return Observable.create({ Observer<String> observer ->
        Thread.start {
            for(articleName in wikipediaArticleNames) {
                try {
                    def t = new URL("https://en.wikipedia.org/wiki/"+articleName).getText()
                    def f = (t =~ /<title>(.*)<\/title>/)
                    observer.onNext(t);
                } catch(Exception e) {
                    observer.onError(e)
                }
            }
            observer.onCompleted();
        }
        return Subscriptions.empty();
    });
}

fetchWikipediaArticleAsynchronouslyWithErrorHandling("Tiger", "Elephant", "NonExistentTitle")
    .subscribe(
        { println "--- Article ---\n" + it.substring(0,125)},
        { println "--- Error ---\n" + it})

