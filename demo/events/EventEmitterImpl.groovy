package demo.events

class EventEmitterImpl implements IEventEmitter {

  def listeners = [:]

  void addListener(String name, IListener listener) {
    if(listeners.containsKey(name)) {
      listeners[name] += listener
    }
    else {
      listeners[name] = [listener]
    }
  }

  void removeListener(String name, IListener listener) {
    if(listeners.containsKey(name)) {
      def removed = listeners[name].remove(listener)
      if(!listeners[name]) {
        listeners.remove(name)
      }
    }
  }

  void removeAllListeners(String name) {
    listeners.remove(name)
  }

  void emit(String name, Object event) {
    if(listeners.containsKey(name)) {
      def listenerList = listeners[name];
      listenerList.each { listener ->
        listener.handle(event)
      }
    }
  }

}
