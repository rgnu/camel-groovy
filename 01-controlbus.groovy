@Grab('org.apache.camel:camel-core:2.16.5')
@Grab('org.apache.camel:camel-netty:2.16.5')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*

def camelContext = new DefaultCamelContext()

camelContext.addRoutes(new RouteBuilder() {
  void configure() {
    from("timer:clock?period=1000")
      .to('controlbus:route?routeId=current&action=status')
      .to("log:camelLogger?showAll=true")
  }
})

camelContext.start();
System.console().readLine();
camelContext.stop();
