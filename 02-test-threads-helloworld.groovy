@Grab('org.apache.camel:camel-core:2.16.5')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import groovy.util.logging.Slf4j


// Beans
@Slf4j
class SystemInfoService {

    def name = 'Test'

    @Handler
    void run(@Body Object body) {
        log.info("${name}: ${body}!")
    }
}

@Slf4j
class CounterService {

    def start = 0
    def step  = 1

    @Handler
    Integer run() {
      start += step
      return start
    }
}

@Slf4j
class SleeperService {
    def time

    @Handler
    void run() {
        log.info("Sleeping ${time}ms ...")
        sleep(this.time);
    }
}


@Slf4j
class TimerStopService {

    def name

    @Handler
    void run(Exchange ex) {
        def time = new Date().time - ex.properties[name]
        log.info("Timer:${name} Lag:${time}...")
    }
}


class TimerStartService {

    def name

    @Handler
    void run(Exchange ex) {
        ex.properties[name] = new Date().time
    }
}



// Vars
def camelContext = new DefaultCamelContext()
camelContext.setMessageHistory(false)
camelContext.setAllowUseOriginalMessage(false)
camelContext.disableJMX()

// Routes
camelContext.addRoutes(new RouteBuilder() {
    void configure() {
        from('timer:clock?period=200')
          .bean(new TimerStartService(name: 'timerTest'))
          .bean(new CounterService())
          .threads(1, 2, 'sleeperQueue').maxQueueSize(1)
            .bean(new SleeperService(time: 1000))
          .end()
          .bean(new TimerStopService(name: 'timerTest'))
    }
})

camelContext.start();
System.console().readLine();
camelContext.stop();
