@Grab('org.apache.camel:camel-core:2.17.7')
@Grab('org.apache.camel:camel-jetty:2.17.7')
@Grab('org.slf4j:slf4j-jdk14:1.7.25')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import org.apache.camel.util.jndi.*
import demo.*

// Vars
def jndiContext  = new JndiContext();
def camelContext = new DefaultCamelContext(jndiContext)

jndiContext.bind('VisitorCounter', new demo.Counter())
jndiContext.bind('HealthCheck',    new demo.HealthCheck());

// Routes
camelContext.addRoutes(new RouteBuilder() {

  void configure() {

    restConfiguration()
    .component('jetty')
    .host('0.0.0.0')
    .port(8090);

    from('timer://jdkTimer?period=3000').routeId('timer')
      .onException(org.apache.camel.http.common.HttpOperationFailedException.class)
        .redeliveryDelay(1000) // 3 seconds
        .maximumRedeliveries(3) // 3 times
        .log(LoggingLevel.WARN, "Http exception noticed")
      .end()
      .setHeader(Exchange.HTTP_URI).simple('https://requestb.in/1ke924b1')
      .setHeader(Exchange.CONTENT_TYPE).constant('text/plain')
      .setBody().constant('OK')
      .to('jetty:localhost')
      .to('log:client?showAll=true')

    rest()
      .description('test')
      .get('/{name}')
    .route()
      .to('log:rest')
      .setBody().constant('OK')
  }
});

camelContext.start();
System.console().readLine();
camelContext.stop();
