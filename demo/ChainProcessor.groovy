package demo;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ChainProcessor implements Processor {

    private ArrayList chain;

    public ChainProcessor(ArrayList<Processor> chain) {
        this.chain = chain
    }

    public ChainProcessor() {
        super([]);
    }

    public void add(Processor p) {
        this.chain.add(p);
    }

    @Override
    public void process(final Exchange exchange) throws Exception {
        this.chain.each({ it.process(exchange); });
    }
}
