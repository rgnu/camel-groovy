package demo.events

public interface IListener {

  String getId();

  void handle(Object event);
}
