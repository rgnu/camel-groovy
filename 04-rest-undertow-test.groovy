@GrabExclude('org.codehaus.groovy:groovy-all')
@Grab('org.apache.camel:camel-core:2.17.7')
@Grab('org.apache.camel:camel-groovy:2.17.7')
@Grab('org.apache.camel:camel-undertow:2.17.7')
@Grab('org.slf4j:slf4j-jdk14:1.7.25')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*

// Vars
def camelContext = new DefaultCamelContext();
camelContext.setMessageHistory(false)
camelContext.setAllowUseOriginalMessage(false)
camelContext.disableJMX()


// Beans
class VisitorCounter {
  Integer num    = 0
  def number     = {num++}

  @Handler
  def String transform(String text) {
    return "You are visitor ${number()}"
  }
}

// Routes
camelContext.addRoutes(new RouteBuilder() {

  void configure() {

    restConfiguration()
    .component('undertow')
    .host('0.0.0.0')
    .port(8090);

    from('rest:get:/test')
    .bean(VisitorCounter.class)
  }

});

camelContext.start();
System.console().readLine();
camelContext.stop();
