@Grab('org.apache.camel:camel-core:2.16.3')

import org.apache.camel.builder.*
import org.apache.camel.ExchangePattern


class TimerRoute extends RouteBuilder {

    @Override
    void configure() {
        from('timer://jdkTimer?period=1000')
            .routeId('timer')
            .to('log:input?showAll=true')
            .setExchangePattern(ExchangePattern.InOnly)
            .to('direct:timer')
            .inOnly()
    }

}
