@Grab('org.slf4j:slf4j-log4j12:1.7.12')
@Grab('com.hazelcast:hazelcast:3.8.4')

import com.hazelcast.config.*
import com.hazelcast.core.Hazelcast
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.core.IQueue

def seed  = 'localhost'
def conf  = new Config()
def joins = conf.getNetworkConfig().getJoin()

joins.getMulticastConfig().setEnabled(false);
joins.getTcpIpConfig().setEnabled(true).addMember(seed);

HazelcastInstance instance = Hazelcast.newHazelcastInstance(conf);

IQueue<Integer> queue = instance.getQueue( "queue" );

while ( true ) {
 int item = queue.take();
 System.out.println( "Consumed: " + item );
 if ( item == -1 ) {
   queue.put( -1 );
   break;
 }
 Thread.sleep( 1000 );
}
System.out.println( "Consumer Finished!" );

instance.shutdown()
