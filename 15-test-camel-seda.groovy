@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.builder.RouteBuilder;

import static demo.Rx.map;
import static demo.Rx.filter;


class ClockPublisher extends RouteBuilder {

  String id     = UUID.randomUUID().toString()

  def listeners = []

  void addListener(listener) {
    log.info(">>> Add ${listener.endpoint}");
    listeners << listener.endpoint
  }

  void removeListener(listener) {
    log.info(">>> Remove ${listener.endpoint}");
    listeners.remove(listener.endpoint)
  }

  Predicate timerFilter(String test) {

    assert test instanceof String

    filter({
        it.in.headers.firedTime.format('MMddHHmmssu').matches(test)
    })
  }


  Processor counter() {

    Integer counter = 0;

    map({
        it.out.body = counter++
    })
  }


  Processor startTime() {

    Integer counter = 0;

    map({
        it.out.body = [start: new Date() ]
    })
  }

  Processor setListenersHeader() {

    map({
        it.in.headers.listeners = listeners.join(',')
    })
  }


  @Override
  void configure() {

    from("timer:clock-${id}")
    .process(startTime())
    .process(setListenersHeader())
    .recipientList(header('listeners'))
    .routeId("clock-${id}");

  }
}


class SleeperSubscriber extends RouteBuilder {

  String id       = UUID.randomUUID().toString()
  String endpoint = "seda:${id}?size=1&concurrentConsumers=1&blockWhenFull=true"

  Processor sleep(Integer time) {
    map({
      sleep(time)
    })
  }

  Processor timeDiff() {
    map({

      assert it.in.body instanceof Map
      assert it.in.body.start instanceof Date

      it.in.body.end = new Date()
      it.in.body.diff = it.in.body.end.time - it.in.body.start.time
    })
  }

  @Override
  void configure() {

    from(endpoint)
    .process(sleep(2000))
    .process(timeDiff())
    .log('${body}')
    .routeId("sleep-${id}");

  }
}






def ctx = new DefaultCamelContext()
ctx.start();

def p   = new ClockPublisher()

def s1  = new SleeperSubscriber()
p.addListener(s1)
ctx.addRoutes(s1)

Thread.start {
	def s2  = new SleeperSubscriber()
	ctx.addRoutes(s2)
	p.addListener(s2)

	sleep(10000)
	p.removeListener(s2)
}

// Event Posting Rest Service
ctx.addRoutes(p)

System.console().readLine();
ctx.stop();
