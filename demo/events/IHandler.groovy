package demo.events

interface IHandler<T> {
  void handle(T event);
}
