@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*

import demo.components.hello.HelloComponent;
import demo.components.counter.CounterComponent;

import demo.Rx;

def logTimeDiff(msg) {
  return Rx.map({
    def timeDiff = new Date().time - it.properties.CamelCreatedTimestamp.time
    println String.format(msg, timeDiff)
  })
}


def ctx = new DefaultCamelContext()
ctx.addComponent('hello', new HelloComponent());
ctx.addComponent('counter', new CounterComponent());

ctx.addRoutes(new RouteBuilder() {
    def void configure() {
        from('hello:foo?option=2000')
        .to('log:input')

        from('counter:foo?start=2000&delay=1000')
        .to('log:input')

        from('timer:test?period=1000')
        .delay(10)
        .process(logTimeDiff("Start ExecTime: %dms"))
        .to('counter:bar')
        .delay(10)
        .process(logTimeDiff("End ExecTime: %dms"))
        .to('log:end?showAll=true')
    }
})

ctx.start();
System.console().readLine();
ctx.stop();
