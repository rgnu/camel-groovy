package demo.activeservice


class ActiveService {

  /**
   * Save the instance $obj
   *
   * @param ActiveRecord $obj The instance to save
   *
   * @return ActiveRecord
   */
  ActiveRecord save(ActiveRecord record) {
  
      if (record.isPersisted()) {
          return this.update(record);
      } else {
          return this.insert(record);
      }
  }

  ActiveRecord insert(ActiveRecord record) {
      record
  }

  ActiveRecord update(ActiveRecord record) {
      record
  }

  ActiveRecord create(Map args) {
    ActiveRecord record = args as ActiveRecord
    record.service = this
    
    return record    
  }
}

