@Grab('org.slf4j:slf4j-log4j12:1.7.5')
@Grab('net.logstash.log4j:jsonevent-layout:1.7')

import groovy.util.logging.Slf4j

@Slf4j
class HelloWorld {

    HelloWorld() {
        log.info 'Hello World'
        throw new Exception('Unspected error')
    }

    static main() {
        new HelloWorld()
    }
}

HelloWorld.main()

