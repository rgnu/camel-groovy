/**
 * ./run 00-start.groovy  | jq -r '"\(.["@timestamp"]) - \(.source_host) - \(.level) - \(.message) - [Error:\(.exception.exception_message)]"'
 */

@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.slf4j:slf4j-log4j12:1.7.5')
@Grab('net.logstash.log4j:jsonevent-layout:1.7')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import org.slf4j.LoggerFactory

import static demo.Rx.map
import static demo.Rx.filter

def ctx = new DefaultCamelContext()
def log = LoggerFactory.getLogger(this.getClass())


ctx.addRoutes(new RouteBuilder() {
    def void configure() {
        from('timer://clock3s?period=3000', 'timer://clock2s?period=2000')

        .onException(Exception.class)
          .handled(true)
          .onRedelivery(map({
            def e = it.properties.CamelExceptionCaught
            log.warn("[ID:${it.in.headers.breadcrumbId}] [Body:${it.in.body}] [Retries:${it.in.headers.CamelRedeliveryCounter}/${it.in.headers.CamelRedeliveryMaxCounter}]", e)
          }))

          .maximumRedeliveries(3)
          .redeliveryDelay(100)
          .useExponentialBackOff()
          .process(map({
            def e = it.properties.CamelExceptionCaught
            log.error("[ID:${it.in.headers.breadcrumbId}] [Body:${it.in.body}]", e)
          }))
        .end()

        .onCompletion()
          .process(map({
            log.info("[ID:${it.in.headers.breadcrumbId}] [Body:${it.in.body}] Completed")
          }))
        .end()
        .bean(demo.Counter.class).id('test')
        .process(map({
            if (it.in.body.toInteger() % 3 == 0) throw new Exception("Unknow error")
        }))
        .process(map({
            def execTime = new Date().time - it.properties.CamelCreatedTimestamp.time
            log.info("[ID:${it.in.headers.breadcrumbId}] [Body:${it.in.body}] Process time ${execTime}ms")
        }))
        .routeId('counter')
    }
})

ctx.start()

addShutdownHook { ctx.stop() }
Thread.currentThread().join()
