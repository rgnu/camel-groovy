package test

@GrabExclude('org.codehaus.groovy:groovy-all')
@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-groovy:2.16.3')
@Grab('org.apache.camel:camel-jsonpath:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import com.jayway.jsonpath.*


//def body   = '{ "item": { "id": 1234, "status": "open", "test": false } }'

def body   = '''{
   "type":"itemClosed",
   "id":"811a5687-07a9-461d-8b97-12d9bebb50af",
   "createdAt":1475607006043,
   "payload":{  
      "item":{  
         "id":888750790,
         "countryId":62,
         "categoryId":812,
         "parentCategoryId":811,
         "date":1467053817,
         "dateToShow":1464634617,
         "ends":1467652140,
         "sellerId":0,
         "locationStateId":7367,
         "locationCityId":70968,
         "languageId":10,
         "deletedAdmin":0,
         "featuredTypeId":0,
         "feeds": false
      }
   }
}'''

//def filter = ".item[?(@.status == 'open')]"
//def filter = ".*"
//def filter = ".item[?(@.status == 'open') && ?(@.id == 1234)]"
//def filter = ".*"
//def filter = '$.payload.item[?(@.feeds == true)]';
def filter = '$.payload.item';
//def filter = 'payload.item[?(@.feeds == true)]'

def result = JsonPath.read(body, filter);

println result
println result.getClass()
println groovy.json.JsonOutput.toJson(result)
println result.isEmpty()
