package demo.components.counter;

import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Counter producer.
 */
public class CounterProducer extends DefaultProducer {
    private static final Logger LOG = LoggerFactory.getLogger(CounterProducer.class);
    private CounterEndpoint endpoint;
    private int counter;

    public CounterProducer(CounterEndpoint endpoint) {
        super(endpoint);
        this.endpoint = endpoint;
        this.counter  = endpoint.getStart();
    }

    public void process(Exchange exchange) throws Exception {
        exchange.getIn().setBody(counter++);
    }

}
