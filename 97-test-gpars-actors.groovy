@GrabExclude('org.codehaus.groovy:groovy-all')
@Grab(group='org.codehaus.gpars', module='gpars', version='1.2.0')

import groovyx.gpars.actor.DynamicDispatchActor
import groovyx.gpars.actor.DefaultActor
import groovyx.gpars.actor.Actor

import org.codehaus.groovy.runtime.NullObject


final class MyActor extends DynamicDispatchActor {
    private int counter = 0

    void onMessage(String message) {
        counter += message.size()
        println 'Received string'
    }

    void onMessage(Integer message) {
        counter += message
        println 'Received integer'
    }

    void onMessage(Object message) {
        counter += 1
        println 'Received object'
    }

    void onMessage(NullObject message) {
        println 'Received a null object. Sending back the current counter value.'
        reply counter
    }
}

final Actor actor = new MyActor().start()

actor.send 1
actor << 2
actor 20
actor 'Hello'
println actor.sendAndWait(null)
