.SILENT:

lib/%.jar: MAVEN_CENTRAL=http://central.maven.org/maven2/
lib/%.jar:
	echo Downloading $@ \
	&& mkdir -p $(CURDIR)/lib \
	&& curl -s -o $(CURDIR)/$@ $(MAVEN_CENTRAL)$(PACKAGE)

.PHONY: %.groovy
%.groovy: lib
	exec java -cp $(CURDIR):$(CURDIR)/resources:$(CURDIR)/lib/* groovy.ui.GroovyMain $@ $(ARGS)

lib/groovy-all-2.4.7.jar: PACKAGE=org/codehaus/groovy/groovy-all/2.4.7/groovy-all-2.4.7.jar
lib/ivy-2.4.0.jar: PACKAGE=org/apache/ivy/ivy/2.4.0/ivy-2.4.0.jar

.PHONY: lib
lib: lib/groovy-all-2.4.7.jar lib/ivy-2.4.0.jar
