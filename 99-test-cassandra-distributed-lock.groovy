@Grab('com.datastax.cassandra:cassandra-driver-core:3.0.7')

import com.datastax.driver.core.Cluster
import com.datastax.driver.core.Host
import com.datastax.driver.core.Metadata

import com.datastax.driver.core.BoundStatement
import com.datastax.driver.core.PreparedStatement


class Lock {
  
  String name
  UUID owner       = UUID.randomUUID()
  Integer ttl      = 180
  Boolean acquired = false

  Boolean acquire() {}  
  Boolean release() {}
  Boolean renew() {}
  
  Boolean isAcquired() { acquired }
  
  @Override
  String toString() {
    "Lock [name: ${name}, owner:${owner}, ttl:${ttl}, acquired:${acquired}]"
  }
}

class CassandraLock extends Lock {
  
  def session
  
  CassandraLock(session, name) {
    this.session = session
    this.name    = name
  }
  
  Boolean acquire() {
    def rows = this.session.execute("INSERT INTO leases (name, owner) VALUES ('${this.name}','${this.owner}') IF NOT EXISTS;")
    return this.acquired = rows.wasApplied()
  }

  Boolean release() {
    if (isAcquired()) {
      def rows      = this.session.execute("DELETE FROM leases where name = '${this.name}' IF owner = '${this.owner}';")
      this.acquired = false
    }  
    return true
  }
  
  Boolean renew() {
    def rows = this.session.execute("UPDATE leases set owner = '${this.owner}' where name = '${this.name}' IF owner = '${this.owner}';")
    return this.acquired = rows.wasApplied()
  }
}



def cluster, locker, session


try {
  cluster = Cluster.builder().addContactPoint('cassandra.cassandra.docker').withPort(9042).build()
  session = cluster.connect('test')
  locker  = new CassandraLock(session, 'test.lock')
  
  if (locker.acquire()) {
    System.console().readLine();
    locker.release();
  }
  println(locker)

} finally {
  cluster?.close();
}
