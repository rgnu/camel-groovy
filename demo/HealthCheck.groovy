package demo

import org.apache.camel.*

class HealthCheck {
  @Handler
  String run() {
    return '{ok}'
  }
}
