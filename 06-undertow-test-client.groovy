@Grab('org.apache.camel:camel-core:2.17.7')
@Grab('org.apache.camel:camel-undertow:2.17.7')
@Grab('org.apache.camel:camel-netty4-http:2.17.7')
@Grab('org.slf4j:slf4j-simple:1.7.25')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import org.apache.camel.util.jndi.*
import org.apache.camel.model.rest.RestBindingMode
import demo.*

// Vars
def jndiContext  = new JndiContext();
def camelContext = new DefaultCamelContext(jndiContext)

jndiContext.bind('VisitorCounter', new demo.Counter())
jndiContext.bind('HealthCheck',    new demo.HealthCheck());

// Routes
camelContext.addRoutes(new RouteBuilder() {

  void configure() {

    restConfiguration()
    .component('netty4-http')
    .bindingMode(RestBindingMode.auto)
    .host('0.0.0.0')
    .port(8090);

    from('timer://jdkTimer?period=3000').routeId('timer')
//      .onException(org.apache.camel.http.common.HttpOperationFailedException)
//        .redeliveryDelay(1000) // 3 seconds
//        .maximumRedeliveries(3) // 3 times
//        .log(LoggingLevel.WARN, "Http exception noticed")
//      .end()
      //.setHeader(Exchange.HTTP_URL).simple('http://localhost:8090/test')
      .setHeader(Exchange.CONTENT_TYPE).constant('application/json')
      .setBody().constant('{ "status": "OK" }')
      .to('netty4-http:http://localhost:8090/pepe')
      .to('log:client')

    rest()
      .description('test').type(Map).consumes('application/json')
      .post('/{name}')
    .route()
      .to('log:rest')
      .setBody().constant('OK')
  }
});

camelContext.start();
System.console().readLine();
camelContext.stop();
