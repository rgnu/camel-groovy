@Grab('org.scala-lang:scala-library:2.11.8')

@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-groovy:2.16.3')
@Grab('org.apache.camel:camel-jetty:2.16.3')
@Grab('org.apache.camel:camel-kafka:2.16.3')
@Grab('org.apache.camel:camel-jackson:2.16.3')
@Grab('org.slf4j:slf4j-jdk14:1.7.5')


//import org.apache.camel.*
import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.util.jndi.JndiContext
import org.apache.camel.component.kafka.KafkaConstants
import org.apache.camel.model.dataformat.JsonLibrary
import org.apache.camel.model.rest.RestBindingMode


class Event {
	String id
	String kind
	Map metadata
	Map payload

	String toString() {
		return "[Kind:${kind}] [Payload:${payload}]"
	}
}


def camelContext = new DefaultCamelContext()


// Event Posting Rest Service
camelContext.addRoutes(new RouteBuilder() {
  void configure() {

    restConfiguration().component('jetty').host('0.0.0.0').port(9797);

    rest('/events/') // Define Event Posting endpoint
      .post()
      .consumes('application/json')
    .route()
      .unmarshal().json(JsonLibrary.Jackson, Event.class)// Convert JSON to PostingEvent class and validate
      .marshal().json(JsonLibrary.Jackson).convertBodyTo(String.class) // Convert PostingEvent class to string
      .setHeader(KafkaConstants.PARTITION_KEY, constant('0'))
      .to('kafka://localhost:9092?topic=events&zookeeperConnect=localhost:2181&serializerClass=kafka.serializer.StringEncoder') // Store Event into posting topic
    .end()
    .routeId('rest.onEventPosting')
  }
});


// Trigger Wallet on Posting Success
camelContext.addRoutes(new RouteBuilder() {
  void configure() {

    from('kafka://localhost:9092?topic=events&groupId=wallet&consumersCount=1&zookeeperConnect=localhost:2181') // Read events from posting topic
      .unmarshal().json(JsonLibrary.Jackson, Event.class) // Convert JSON to PostingEvent class
      .filter(simple('${body.kind} == "PostingSuccess"')) // Filter Posting Success
      .to('log:input?showAll=true')
    .end()
    .routeId('wallet.onPostingSucess')
  }
});


camelContext.start();
System.console().readLine();
camelContext.stop();
