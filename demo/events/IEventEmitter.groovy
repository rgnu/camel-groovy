package demo.events

public interface IEventEmitter {

  //methods to register and unregister observers
  void addListener(String name, IListener listener);
  void removeListener(String name, IListener listener);
  void removeAllListeners(String name);

  //method to notify observers of change
  void emit(String name, Object event);
}
