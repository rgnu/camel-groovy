@GrabExclude('org.codehaus.groovy:groovy-all')
@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-groovy:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*

def ctx = new DefaultCamelContext()
def p = ctx.createProducerTemplate()

def fromJson() {
  slurper = new groovy.json.JsonSlurper();
  return {
    it.in.body = slurper.parseText(it.in.body);
  }
}

def toJson() {
  return {
    it.in.body = groovy.json.JsonOutput.toJson(it.in.body);
  }
}

ctx.addRoutes(new RouteBuilder() {
    def void configure() {
        from('direct:input')
          .process(fromJson())
          .filter(simple('${body["person"]["age"]} > 20'))
          .process(toJson())
          .to('log:filtered')
        .end()
        .routeId('groovy')
    }
})

ctx.start();

p.sendBody('direct:input','{"person":{"name":"Guillaume","age":33,"pets":["dog","cat"]}}');
p.sendBody('direct:input','{"person":{"name":"Pepe","age":18,"pets":["dog"]}}');
p.sendBody('direct:input','{"person":{"name":"Lola","age":90,"pets":[]}}');

ctx.stop();
