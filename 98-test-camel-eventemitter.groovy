@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.builder.RouteBuilder;

import demo.events.*

class Clock {

    private Consumer consumer
    private Endpoint endpoint
    private String id

    Clock(CamelContext ctx, IEventEmitter eventEmitter) {
      this.id       = UUID.randomUUID().toString();
      this.endpoint = ctx.getEndpoint("timer:${id}")

      this.consumer = this.endpoint.createConsumer(new Processor() {
          public void process(Exchange exchange) throws Exception {
              eventEmitter.emit('clock', exchange)
          }
      });

      ctx.addService(consumer);
    }

    String getId() {
      return id
    }
}


class LoggerListener implements IListener {

    private String id;

    LoggerListener() {
      this(UUID.randomUUID().toString());
    }

    LoggerListener(String id) {
      this.id = id
    }

    String getId() {
      return id;
    }

    void handle(Object event) {
      println "${id} receive Object ${event}";
    }

    void handle(String event) {
      println "${id} receive String ${event}";
    }

    void handle(Exchange event) {
      println "${id} receive Exchange Headers:${event.in.headers}";
    }

}


def ctx = new DefaultCamelContext()

IEventEmitter eventEmitter = new EventEmitterImpl();
IListener l1 = new LoggerListener('l1');
IListener l2 = new LoggerListener('l2');

eventEmitter.addListener('clock', l1);
eventEmitter.addListener('clock', l2);

Clock c = new Clock(ctx, eventEmitter);

ctx.start();

System.console().readLine();
ctx.stop();