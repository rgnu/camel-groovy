@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-jetty:2.16.3')
@Grab('org.slf4j:slf4j-jdk14:1.7.5')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import org.apache.camel.util.jndi.*
import demo.*

// Vars
def jndiContext  = new JndiContext();
def camelContext = new DefaultCamelContext(jndiContext)

jndiContext.bind('VisitorCounter', new demo.Counter())
jndiContext.bind('HealthCheck',    new demo.HealthCheck());

// Routes
camelContext.addRoutes(new RouteBuilder() {

  void configure() {

    restConfiguration().component('jetty').host('0.0.0.0').port(8090);

    from('timer://jdkTimer?period=3000').routeId('timer')
      .onException(org.apache.camel.http.common.HttpOperationFailedException.class)
        .redeliveryDelay(3*1000) // 3 seconds
        .maximumRedeliveries(3) // 3 times
        .log(LoggingLevel.WARN, "Http exception noticed")
      .end()
      .to('jetty:http://localhost:8090/health')

    rest()
      .description('health')
      .get('/health')
      .to('bean:HealthCheck')

    rest().path('/visitor')
      .get()
        .description('visitor.get')
        .to('bean:VisitorCounter?method=doGet')
      .post()
        .description('visitor.incr')
        .to('bean:VisitorCounter?method=doIncr')
      .delete()
        .description('visitor.decr')
        .to('bean:VisitorCounter?method=doDecr')
  }
});

camelContext.start();
System.console().readLine();
camelContext.stop();
