@GrabExclude('org.codehaus.groovy:groovy-all')
@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-groovy:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*

def ctx = new DefaultCamelContext()
def p = ctx.createProducerTemplate()

ctx.addRoutes(new RouteBuilder() {
    def void configure() {
        from('direct:input')
          .transform { new Random().nextInt(1000) }
          .process { println it }
          // .filter { Exchange e -> e.in.body > 500 } // Dont Work
        .end()
        .routeId('groovy')
    }
})

ctx.start();

p.sendBody('direct:input', '{"status": "ok", "text": "pass1"}')
p.sendBody('direct:input', '{"status": "fail", "text": "nop"}')
p.sendBody('direct:input', '{"status": "ok", "text": "pass2"}')

ctx.stop();
