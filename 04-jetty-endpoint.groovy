@Grab('org.apache.camel:camel-core:2.13.1')
@Grab('org.apache.camel:camel-groovy:2.13.1')
@Grab('org.apache.camel:camel-jetty:2.13.1')
@Grab('org.slf4j:slf4j-jdk14:1.7.5')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*

// Vars
def camelContext = new DefaultCamelContext();

// Beans
class VisitorCounter {
	Integer num    = 0
	def number     = {num++}

  @Handler
  def String transform(String text) {
    return "You are visitor ${number()}"
  }
}

// Routes
camelContext.addRoutes(new RouteBuilder() {

  void configure() {

    restConfiguration()
    .component('jetty')
    .host('0.0.0.0')
    .port(8090);

    from('jetty:http://0.0.0.0:8090/')
    .bean(VisitorCounter.class)

    rest()
    .post('/queue/{name}')
    .to('log:input?showAll=true')

  }

});

camelContext.start();
System.console().readLine();
camelContext.stop();
