@Grab('io.projectreactor:reactor-stream')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import reactor.rx.*
import reactor.Environment

def env     = Environment.initializeIfEmpty()
def counter = Streams.period(1)
def power   = counter.map({ it * it })
def sleeper = power.map({ sleep(1000); it})

counter
.consume({ println "Counter: $it" })

power
.consume({ println "Power  : $it" })


Streams.join(counter, sleeper)
.map({ [counter: it[0], power: it[1]] })
.consume({ println "Results: $it" })

System.console().readLine()
