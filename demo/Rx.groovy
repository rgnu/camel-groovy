package demo

@Grab('org.apache.camel:camel-core:2.16.3')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*


class Rx {

    public static void install() {
        org.apache.camel.model.ProcessorDefinition.metaClass.map = { Closure callback ->
            delegate.process(new Processor() {
                void process(Exchange ex) {
                    callback(ex)
                }
            })
        }

        org.apache.camel.model.ProcessorDefinition.metaClass.filter = { Closure<Boolean> callback ->
            delegate.filter(new Predicate() {
                boolean matches(Exchange ex) {
                    return callback(ex)
                }
            })
        }
    }

    public static Processor map(Closure callback) {
      return new Processor() {
        void process(Exchange ex) {
          callback(ex)
        }
      }
    }

    public static Processor ClosureProcessor(Closure callback) {
      return map(callback)
    }


    public static Predicate filter(Closure<Boolean> callback) {
      return new Predicate() {
        boolean matches(Exchange ex) {
          return callback(ex)
        }
      }
    }

    public static Predicate ClosurePredicate(Closure<Boolean> callback) {
      return filter(callback)
    }


    public static Closure log() {
      return { println it }
    }

    public static Closure counter(Integer start) {
      return { it.in.body = start++ }
    }

}

