@Grab('org.scala-lang:scala-library:2.11.11')
@Grab('org.apache.camel:camel-core:2.16.5')
@Grab('org.apache.camel:camel-kafka:2.16.5')
@Grab('org.apache.camel:camel-jackson:2.16.5')
@Grab('org.slf4j:slf4j-simple')


import org.apache.camel.*
import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.kafka.KafkaConstants
import org.apache.camel.model.dataformat.JsonLibrary


class KafkaRoute extends RouteBuilder {

  private counter = 0;
  private slurper = new groovy.json.JsonSlurper();

  @Override
  void configure() {
    def kafka = new URI(System.properties.get('kafka.uri', 'kafka://kafka.magneto.docker:9092'))
    def zk    = new URI(System.properties.get('zookeeper.uri', 'zookeeper://zookeeper.magneto.docker:2181,zookeeper.magneto.docker:2181'))

    def timer  = this.context.getEndpoint('timer:clock?period=100')
    def queue  = this.context.getEndpoint('seda:queue?size=1&concurrentConsumers=50&blockWhenFull=true')
    def kafkaC = this.context.getEndpoint("kafka:?zookeeperConnect=${zk.authority}&brokers=${kafka.authority}&topic=testEvent&groupId=testKafka&consumersCount=1")

    def kafkaP = this.context.getEndpoint("kafka:?zookeeperConnect=${zk.authority}&brokers=${kafka.authority}&topic=testEvent&serializerClass=kafka.serializer.StringEncoder&autoCommitIntervalMs=1000")

    from(kafkaC).routeId('events.rx')
    .to(queue)

    from(timer).routeId('events.tx')
    .bean(this, 'newMessage')
    .bean(this, 'jsonEncode')
    .setHeader(KafkaConstants.KEY, header('breadcrumbId'))
    .to(kafkaP)


    from(queue).routeId('events.queue')
    .bean(this, 'toJson')
    .bean(this, 'sleeper')
    .bean(this, 'timeDiff')
    .bean(this, 'printMessage')

  }


  Map timeDiff(@Body Map message) {
    message.diff = new Date().getTime() - message.timestamp
    //sleep(100)
    return message
  }

  Map newMessage(@Header("breadcrumbId") String id, Exchange e) {
    return [ kind: 'Clock', id: id, timestamp: new Date().getTime(), counter: this.counter++ ]
  }

  Map printMessage(@Body Map message, Exchange ex) {
    log.info("king:${message.kind} id:${message.id} counter:${message.counter} age:${message.diff} date:${new Date(message.timestamp)}")
    return message
  }

  String toString(@Body String body) {
    body
  }

  String jsonEncode(Exchange ex) {
    groovy.json.JsonOutput.toJson(ex.in.body);
  }

  Map toJson(Exchange ex) {
     return slurper.parse(ex.in.body);
  }

  void sleeper(Exchange ex) {
    log.debug("Sleeping....")
    sleep(1000)
  }

  void logger(Exchange ex) {
    def bodyType = ex.in.body?.getClass()

    log.info("[Route:${ex.fromRouteId}] [Headers:${ex.in.headers}] [BodyType:${bodyType}] [Body:${ex.in.body}]")
  }

}


def ctx = new DefaultCamelContext()

// Event Posting Rest Service
ctx.addRoutes(new KafkaRoute())

ctx.start();
System.console().readLine();
ctx.stop();
