@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-zookeeper:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*

class Zk {

  ArrayList findAll(CamelContext ctx, String id) {
    def c    = ctx.createConsumerTemplate()
    def body = c.receiveBody("zookeeper://localhost:2181/${id}?listChildren=true", ArrayList.class)
    c.stop()

    return body
  }

  String findById(CamelContext ctx, String id) {
    def c    = ctx.createConsumerTemplate()
    def body = c.receiveBody("zookeeper://localhost:2181/${id}", String.class)
    c.stop()

    return body
  }

}

class TestRouter extends RouteBuilder {
  @Override
  void configure() {

    from('zookeeper://localhost:2181/test/somenode?repeat=true')
    .transform().simple('test/somenode')
    .bean(Zk.class, 'findAll').split().body()
    .transform().simple('test/somenode/${body}')
    .log('Process entry ${body} [ID:${id}]')
    .bean(Zk.class, 'findById')
    .log('Content ${body} [ID:${id}]')
  }
}


// Vars
def camelContext = new DefaultCamelContext()

// Routes
camelContext.addRoutes(new TestRouter());

camelContext.start();
System.console().readLine();
camelContext.stop();
