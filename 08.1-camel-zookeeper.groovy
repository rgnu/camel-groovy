@Grab('org.apache.camel:camel-core:2.16.3')
@Grab('org.apache.camel:camel-zookeeper:2.16.3')
@Grab('org.apache.camel:camel-jackson:2.16.3')
@Grab('org.slf4j:slf4j-simple:1.7.12')

import org.apache.camel.*
import org.apache.camel.impl.*
import org.apache.camel.builder.*
import org.apache.camel.util.jndi.*
import org.apache.camel.processor.aggregate.*
import org.apache.camel.model.dataformat.JsonLibrary


class TestRouter extends RouteBuilder {
  @Override
  void configure() {

    from('zookeeper://localhost:2181/test/somenode?repeat=true')
    .convertBodyTo(String.class)
    .unmarshal().json(JsonLibrary.Jackson, ArrayList.class)
    .split().body()
    .to('log:input?showAll=true')

  }
}


// Vars
def camelContext = new DefaultCamelContext()

// Routes
camelContext.addRoutes(new TestRouter());

camelContext.start();
System.console().readLine();
camelContext.stop();
