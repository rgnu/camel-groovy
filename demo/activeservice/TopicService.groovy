package demo.activeservice

class TopicService extends ActiveService {

  def    session
  String table = "topic"
  
  
  TopicService(session) {
    this.session = session
  }
  
  ActiveRecord insert(TopicRecord record) {
    
    def res = this.session.execute("INSERT INTO ${this.table} (topic, offset, data) VALUES ('${record.topic}', ${record.offset ?: 'now()'}, '${record.data}') USING TTL 3600;")
    
    res.wasApplied() || new RuntimeException("Error Saving ${record}")
    
    return record
  }

  def findLatestByTopicAndOffset(String topic, UUID offset, Integer limit) {
    
    def res = this.session.execute("SELECT topic, offset, data FROM ${this.table} WHERE topic = '${topic}' AND offset > ${offset} AND offset < now() LIMIT ${limit}");
    
    res.wasApplied() || new RuntimeException("Error Saving ${record}")
    
    return res
  }
  
  ActiveRecord create(Map args) {
    ActiveRecord record = args as TopicRecord
    record.service = this
    
    return record
  }
}

