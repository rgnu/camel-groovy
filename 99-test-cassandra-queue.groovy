/*
    CREATE KEYSPACE IF NOT EXISTS queue WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'}  AND durable_writes = true;

    CREATE TABLE IF NOT EXISTS queue.topic (
        topic text,
        offset timeuuid,
        data text,
        PRIMARY KEY (topic, offset)
    ) WITH CLUSTERING ORDER BY (offset ASC)
        AND compaction = {'class': 'org.apache.cassandra.db.compaction.TimeWindowCompactionStrategy', 'compaction_window_size': '60', 'compaction_window_unit': 'MINUTES', 'max_threshold': '32', 'min_threshold': '4'}
        AND default_time_to_live = 3600
        AND gc_grace_seconds = 600;

    CREATE TABLE IF NOT EXISTS queue.consumer (
        id text,
        topic text,
        offset timeuuid,
        PRIMARY KEY (id, topic)
    ) WITH CLUSTERING ORDER BY (topic ASC);

*/

@Grab('com.datastax.cassandra:cassandra-driver-core:3.0.7')

import com.datastax.driver.core.Cluster
import com.datastax.driver.core.Host
import com.datastax.driver.core.Metadata
import com.datastax.driver.core.utils.UUIDs

import com.datastax.driver.core.BoundStatement
import com.datastax.driver.core.PreparedStatement

import demo.activeservice.*


def cluster = Cluster.builder().addContactPoint("cassandra.cassandra.docker").withPort(9042).build()

try {

    def session         = cluster.connect("queue")
    def topicService    = new TopicService(session)
    def consumerService = new ConsumerService(session)
    def random          = new Random()
    def consumer        = consumerService.create([id: "consumer", topic: "test.0", offset: UUIDs.timeBased()])
    
    def startTime       = new Date().time
    def counter         = 100000
    def text            = "0123456789" * 200
    
    while (counter--) {
      def now = new Date()
      
      topicService.create(
        [
          topic: "test.${random.nextInt(100)}", 
          data: [
            date:        now,            
            freeMemory:  Runtime.runtime.freeMemory(), 
            totalMemory: Runtime.runtime.totalMemory(),
            maxMemory:   Runtime.runtime.maxMemory(),
            random:      random.nextInt(10000),
            text:        text
          ]
        ]
      ).save()
      
      if (counter % 10000 == 0) {
        def timeDiff = now.time - startTime; startTime = now.time
        println("Counter:${counter} Time:${timeDiff} Rps:${10000/(timeDiff/1000)}")
      }

/*
      def timeDiff = new Date().time - now.time
      
      topicService.findLatestByTopicAndOffset(consumer.topic, consumer.offset, 100).each {
        println("${consumer.id} ${it.getString('data')}")
        consumer.offset = it.getUUID('offset')
      }
      
      println("Sleeping 100ms Time:${timeDiff}")
      sleep(100)
*/
    }

} finally {
    cluster?.close()
}
